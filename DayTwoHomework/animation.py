import os
import time
import random

def randChar(inString):
    return random.choice(inString)

def randString(n):
    outString = ""
    for i in range(n):
        outString = outString + str(randChar("qwertyuiopasdfghjklzxcvbnm"))
    return outString
#using space and/or empty line to change location of your figure
def figure(n):
    line0 =      str(randString(n)) + "\n"
    line1 =      str(randString(n)) + "\n"
    line2 =      str(randString(n)) + "\n"
    line3 =      str(randString(n)) + "\n"
    line4 =      str(randString(n)) + "\n"
    line5 =      str(randString(n)) + "\n"
    line6 =      str(randString(n)) + "\n"
    line7 =      str(randString(n)) + "\n"
    line8 =      str(randString(n)) + "\n"
    line9 =      str(randString(n)) + "\n"
    line10=      str(randString(n)) + "\n"
    line11=      str(randString(n)) + "\n"
    line12=      str(randString(n)) + "\n"
    line13=      str(randString(n)) + "\n"
    line14=      str(randString(n)) + "\n"
    f=line0+line1+line2+line3+line4+line5+line6+line7+line8+line9+line10+line11+line12+line13+line14
    print(f)
      

#this functions as clear the console screen (remove anything shown in curren screen)
clear = lambda: os.system('cls')

#clear screen before print image
clear()

#run the figure function 1000 times, each time, figure funtion will print images in different location and then clear screen before we run next figure function.
for i in range(0,1000):
    n=i

    figure(n)

    #the printing of figure will keep on screen for 0.5 second, and then the clear function will clear printing
    #you can set time to control the speed of animation.
    time.sleep(0.5)

    #clear screen before we start next loop    
    clear()
    
